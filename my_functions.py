# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :functions.py
#description     :Funciones utiles
#version         :1.0
#usage           :python gtt-cloud-run.py
#author          :Juan Pablo Chiabrando
#copyright       :Copyright© 2021 GlobalThink Technology S.A.
#===============================================================================

__author__ = 'Juan Pablo Chiabrando'
__collaborator__ = ' '
__copyright__ = 'Copyright© 2021 GlobalThink Technology S.A.'
__version__ = "1.0"
__status__ = "Production"


from termcolor import colored, cprint
import sys
import commands
import os

#Escribe un titulo
def titulo(TEXTO):
	c= len(TEXTO)
	SEPARADOR= '-' * c
	cprint(SEPARADOR, 'white', attrs=['bold'])
	cprint(TEXTO, 'blue', attrs=['bold'])
	cprint(SEPARADOR, 'white', attrs=['bold'])
	print ("")


#Subtitulo
def subtitulo(TEXTO):
	c= len(TEXTO)
	SEPARADOR= '.' * c
	cprint(SEPARADOR, 'white', attrs=['bold'])
	cprint(TEXTO, 'white')
	cprint(SEPARADOR, 'white', attrs=['bold'])
	print ("")

#Aclaracion, pueden ser dos textos
def aclaracion(TEXTO1,TEXTO2):
	b= len(TEXTO1)
	c= len(TEXTO2)
	if c > b:
		b = c
	SEPARADOR= '-' * b
	cprint(SEPARADOR, 'white', attrs=['bold'])
	cprint("IMPORTANTE", 'red', attrs=['bold'])
	cprint(TEXTO1, 'white', attrs=['bold'])
	cprint(TEXTO2, 'white', attrs=['bold'])
	cprint(SEPARADOR, 'white', attrs=['bold'])
	print ("")


#Validador de opcion elegida
def validator(TEXTO1,TEXTO2):
	print(TEXTO1)
	val = raw_input("Desea continuar? [s]/[n]\n")
	if (val is "n"):
		print ("")
		print(TEXTO2)
		print ("")
		sys.exit(0)

#Da dos espacios
def espacios():
	print (" ")
	print (" ")


#Guarda las variables en el archivo dado p el TEXTO en el ARCHIVO
def var2file(TEXTO,ARCHIVO):
	f = open(ARCHIVO, "a") #
	f.write(str("%s\n"%TEXTO))
	f.close()

#Seteando el archivo de variables
def cleanvars():
	f = open("deploy-vars", "w") 
	f.write("[Variables]\n")
	f.close()


#Seleccion de proyectos
def project_selector():
	titulo("Seleccion de proyecto de trabajo")
	#Listando y acomodando proyectos diponibles 
	PROJECTS=commands.getoutput('gcloud projects list --format="table[text](PROJECT_ID)" | grep -v PROJECT_ID')
	A=PROJECTS.splitlines()
	subtitulo("lista de proyecto disponibles")
	print(A)
	for x in range(0,len(A)):
		#print A[x]
		print("[%d] = %s" % (x, A[x]))
	#Eligiendo el proyecto de trabajo
	print("")
	OPT = int(input("Elgir el numero correspondiente del proyecto a usar: "))
	PROJECT=A[OPT]
	cprint(PROJECT, 'blue', attrs=['bold'])
	#Guardando en archivo
	VARIABLE=('PROJECT=%s' %PROJECT)
	var2file(VARIABLE,'deploy-vars')
	#Seteando el proyecto en gcloud
	PROJECTS_SET=commands.getoutput('gcloud config set project %s' %PROJECT)
	return PROJECT


#Seleccion de entornos de trabajo
def scope():
	ENTORNOS=['TESTING','PRODUCCION']

	subtitulo("Seleccion de Scopes de trabajo")

	for x in range(0,len(ENTORNOS)):
		print("[%d] = %s" % (x+1, ENTORNOS[x]))

	#Eligiendo la imagen de trabajo
	print("")
	OPT = int(input("Seleccione el scope de la implementacion: "))
	cprint(ENTORNOS[OPT-1], 'blue', attrs=['bold'])
	if OPT == 1:
		SCOPE='test'
	elif OPT == 2:
		SCOPE='prod'
	else:
		print ("No existe esa opcion \nEjecutar nuevamente")
		sys.exit(1)
	return SCOPE


#Seleccion de imagen y tag disponible
def image_tag():
	#colocando titulo
	espacios()
	titulo("Eleccion de imagen a deployar")
	##Listando y acomodando imagenes diponibles 
	IMAGES=commands.getoutput('gcloud container images list | grep -v NAME')
	A=IMAGES.splitlines()
	subtitulo("Imagenes disponibles del proyecto")
	for x in range(0,len(A)):
		#print A[x]
		if ("--repository" in A[x] ):
			#print ("")
			pass
		else:
			print("[%d] = %s" % (x, A[x]))
	#Eligiendo la imagen de trabajo
	print("")
	OPT = int(input("Elegir el numero correspondiente de la IMAGEN a utilizar: "))
	IMAGE=A[OPT]
	cprint(IMAGE, 'blue', attrs=['bold'])
	espacios()
	#Guardando en archivo la imagen
	VARIABLE=('IMAGE=%s' %IMAGE)
	var2file(VARIABLE,'deploy-vars')
	##Listando y acomodando los tags diponibles 
	TAGS=commands.getoutput('gcloud container images list-tags %s --format="table[text](TAGS)" | grep -v TAGS' %IMAGE)
	B=TAGS.splitlines()
	subtitulo("Tags disponibles de la imagen seleccionada")
	for x in range(0,len(B)):
		print("[%d] = %s" % (x+1, B[x]))
	#Eligiendo la imagen de trabajo
	print("")
	OPT = int(input("Elegir el numero correspondiente del TAG a utilizar: "))
	TAG=B[OPT-1]
	cprint(TAG, 'blue', attrs=['bold'])
	IMAGE_TAG=('%s:%s' %(IMAGE,TAG))
	#Guardando en archivo la imagen y el tag
	VARIABLE=('IMAGE_TAG=%s' %IMAGE_TAG)
	var2file(VARIABLE,'deploy-vars')
	return IMAGE, IMAGE_TAG

def vars2yml(ARCHIVO):
	f = open(ARCHIVO, "r")
	for linea in f:
		if linea is '': 
			pass
		else:
			print(linea)
	f.close()
	# vars2yml('/home/denwaing/ing/bitbucket/DevOPS/gtt-cloud_run/v2/env_funtions')


#seleccion de variables de entorno
def env_selector(TIPO):
	RUTA_ENV = raw_input("Colocar la ubicacion del archivo de variables correspodiente: \n")
	TIPE=TIPO
	CAR_ESP=['#','{']
	VARS_DELETE= []
	LINE_DELETE= []
	ENV_FILE= []
	C=0
	f = open(RUTA_ENV, "r")
	#lines = f.readlines()
	for linea in f:
		C=C+1
		for x in range(0,len(CAR_ESP)):
			if CAR_ESP[x] in linea:
				VARS_DELETE.append(linea)
				LINE_DELETE.append(C)
			else:
				pass
	#Eliminando filas del env
	f.close()
	f = open(RUTA_ENV, "r")
	lines = f.readlines()
	f.close()
	if not LINE_DELETE:
		pass
	else:
		for y in range(-1,-len(LINE_DELETE)-1,-1):
			B=LINE_DELETE[y]
			del lines[B-1]
			if TIPE is 'function':
				new_file = open("./.env.yaml", "w+")
				for line in lines:
				    new_file.write(line.replace('=',' : ',1))
				new_file.close()
	#Guardando en archivo las variables y las varaibles eliminadas
	VARIABLE=('ENV_VARS=%s' %lines)
	var2file(VARIABLE,'deploy-vars')
	VARIABLE=('VARS_DELETE=%s' %VARS_DELETE)
	var2file(VARIABLE,'deploy-vars')
	return lines

def tool_path():
	os.chdir('/usr/local/gtt-gcp-tool')
