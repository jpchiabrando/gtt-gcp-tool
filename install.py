# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :gtt-cloud-run.py
#description     :Menu selector de opciones 
#version         :1.0
#usage           :python gtt-cloud-run.py
#===============================================================================
#


import os
import sys
import time
from my_functions import *

def imprimir():
	print " "
	print "-----------------"
	print "- Instalando... -"
	print "-----------------"
	print " "
	print " "
	print "Elija que sistema operativo utiliza"
	print("")
	print("[1] = MAC OS")
	print("[2] = Linux")
	print("[0] = exit")
	print("")

def menu():
	while True:
		imprimir()
		try:
			entrada_usuario = int(input("Seleccione una opcion: "))
			if entrada_usuario in range(4):
				if entrada_usuario == 0:
					print("")
					print("Adios! Vuelva pronto")
					print("")
					break
				elif entrada_usuario == 1:
					os.system ("curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py")
					os.system ("python get-pip.py")
					os.system ("pip install termcolor")
					break
				elif entrada_usuario == 2:
					os.system ("pip install termcolor")
					break
			else:
				print('Error, solo de aceptan numeros del 0 al 2')

		except ValueError:
			print("Error, ingrese solamente numeros")

#RECORDAR SI SE ACTUALIZA EL PATH DE INSTALACION ES NECESARIO ACTUALIZAR EN myfunctions.py
def basic_install():
	os.system ("mkdir -p /usr/local/gtt-gcp-tool")
	os.system ("cp -r ../gtt-gcp-tool/* /usr/local/gtt-gcp-tool/")
	os.system ("cp command-console /usr/local/bin/gtt-gcp-tool")
	os.system ("chmod +x /usr/local/bin/gtt-gcp-tool")


if __name__ == '__main__':
	menu()
	basic_install()
	espacios()
	titulo('Instalacion exitosa')

