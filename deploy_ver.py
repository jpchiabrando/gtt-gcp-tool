# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :gtt-cloud-run.py
#description     :Menu selector de opciones 
#version         :1.0
#usage           :python gtt-cloud-run.py
#===============================================================================

__author__ = 'Juan Pablo Chiabrando'
__collaborator__ = ' '
__copyright__ = 'Copyright© 2021 GlobalThink Technology S.A.'
__version__ = "1.0"
__status__ = "Production"

import sys
from termcolor import colored, cprint

print colored('hello', 'red'), colored('world', 'green')


text = colored('Hello, World!', 'red', attrs=['reverse', 'blink'])
print(text)
cprint('Hello, World!', 'green', 'on_red')

print_red_on_cyan = lambda x: cprint(x, 'red', 'on_cyan')
print_red_on_cyan('Hello, World!')
print_red_on_cyan('Hello, Universe!')

cprint("IMPORTANTE", 'red', attrs=['bold'])

for i in range(10):
    cprint(i, 'magenta', end=' ')

cprint("Attention!", 'red', attrs=['bold'], file=sys.stderr)