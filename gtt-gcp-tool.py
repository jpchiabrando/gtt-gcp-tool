# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :gtt-cloud-run.py
#description     :Menu selector de opciones 
#version         :1.0
#usage           :python gtt-cloud-run.py
#===============================================================================

__author__ = 'Juan Pablo Chiabrando'
__collaborator__ = ' '
__copyright__ = 'Copyright© 2021 GlobalThink Technology S.A.'
__version__ = "1.0"
__status__ = "Production"


import os
import sys
from my_functions import *

clear = lambda: os.system('clear')
clear()


def imprimir():
    subtitulo("Que dese hacer?")
    print("")
    print("[1] GCP Cloud Run deployment")
    print("[2] GCP Cloud Function deployment")
    print("[3] Upload a container image to GCP")
    print("[4] Create infrastructure for Cloud Run on GCP")
    print("[0] exit")
    print("")


def user_check():
    print("")
    os.system ('gcloud config configurations list')
    print("")
    validator('El usuario marcado con TRUE es con el que desea operar?','Ejecute el siguiente comando para cambiar de cuenta\ngcloud config configurations activate  "ACCOUNT_NAME"')
    print("")


def menu():
    while True:
        imprimir()
        try:
            entrada_usuario = int(input("Seleccione una opcion: "))

            if entrada_usuario in range(4):

                if entrada_usuario == 0:
                    print("")
                    print("Adios! Vuelva pronto")
                    print("")
                    break
                elif entrada_usuario == 1:
                    os.system ("python deploy-cloud-run.py")
                    break
                elif entrada_usuario == 2:
                    os.system ("python deploy-cloud-function.py")
                    break
                elif entrada_usuario == 3:
                    os.system ("python upload_img_gcp.py")
                    break
            else:
                print('Error, solo de aceptan numeros del 0 al 4')

        except ValueError:
            print("Error, ingrese solamente numeros")


if __name__ == '__main__':
    tool_path()
    titulo("Herramienta de deployment en Cloud Run")
    aclaracion('Es necesario tener instalada la herramientas SDK de GCP','https://cloud.google.com/sdk/docs/quickstart#mac')
    user_check()
    menu()