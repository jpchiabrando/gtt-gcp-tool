# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :env_selector.py
#description     :Selector de imagenes para GCP 
#version         :1.0
#usage           :python env_selector.py
#===============================================================================

import os
#import shutil
from my_functions import *
from termcolor import cprint
import commands
#import sys

#Seteos
cleanvars()


#Seleccion de proyecto
espacios()
PROJECT=project_selector()


#Seleccion de region
espacios()
subtitulo("Seteo de region")
REGION="us-central1"

print ("La region utilizada por defecto:")
cprint(REGION, 'blue', attrs=['bold'])


#Seleccion de RUNTIME
espacios()
A=['nodejs10','nodejs12','nodejs14','python37','python38','python39','go111','go113','java11','dotnet3','ruby26']

subtitulo("lista de RUNTIMEs disponibles")
for x in range(0,len(A)):
	#print A[x]
	print("[%d] = %s" % (x+1, A[x]))
#Eligiendo el RUNTIME de trabajo
print("")
OPT = int(input("Elegir el numero correspondiente del RUNTIME a usar: "))
RUNTIME=A[OPT-1]
cprint(RUNTIME, 'blue', attrs=['bold'])


#Seleccion de SCOPE
espacios()
SCOPE=scope()


#Nombre de la funcion
espacios()
subtitulo('Nombre de la aplicacion')
NAME=raw_input("Colocar el nombre de la aplicacion (debe coincidir con el repositorio): \n")
NAME=PROJECT+'-'+NAME+'-'+SCOPE


#working dir
espacios()
subtitulo('Seleccion de path de trabajo')
RUTA=raw_input("Colocar path del repo correspodiente: \n")
os.chdir(RUTA)


#Seteando variables de entorno
espacios()
subtitulo('Seteo de variables de entorno')
env_selector('function')

#Deployando
espacios()
os.system ('gcloud functions deploy %s --runtime=%s --region us-central1 --trigger-http --entry-point=handler --env-vars-file .env.yaml' % (NAME, RUNTIME))

