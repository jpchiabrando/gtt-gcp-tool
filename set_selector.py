# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :set_selector.py
#description     :Selector de configuraciones para GCP 
#version         :1.0
#usage           :python set_selector.py
#===============================================================================


#import os
#import shutil
from my_functions import *
#from termcolor import cprint
#import commands
#import sys

#colocando titulo
espacios()
titulo("Seteos de configuraciones fijas")

#Seteo de region
subtitulo("Seteo de region")
REGION="us-central1"

print ("La region utilizada por defecto:")
cprint(REGION, 'blue', attrs=['bold'])

#Guardando en archivo el puerto
VARIABLE=('REGION=%s' %REGION)
var2file(VARIABLE,'deploy-vars')


#Seteo de puerto
subtitulo("Seteo de puerto")
PORT="8080"

print ("Puerto utilizado por defecto:")
cprint(PORT, 'blue', attrs=['bold'])

#Guardando en archivo el puerto
VARIABLE=('PORT=%s' %PORT)
var2file(VARIABLE,'deploy-vars')


#Seteo de Acceso
print ("")
subtitulo("Seteo de Acceso")

print ("Acceso al webservice restrigido a tafrico local por defecto:")
val = raw_input("Se recomienda no activar el acceso libre\nDesea Habilitar el acceso libre al webservice? [s]/[n]\n")
if (val is "s"):
	print ("")
	ACCESS="--allow-unauthenticated"
elif (val is "n"):
	ACCESS="--no-allow-unauthenticated"

#Guardando en archivo el tipo de acceso
VARIABLE=('ACCESS=%s' %ACCESS)
var2file(VARIABLE,'deploy-vars')



subtitulo("Seteo de cantidad de instancias")
MIN_INSTANCES="0"
MAX_INSTANCES="5"

print ("")
print ("Cantidad minima de instancias por defecto:")
cprint(MIN_INSTANCES, 'blue', attrs=['bold'])

print ("")
print ("Cantidad maxima de instancias por defecto:")
cprint(MAX_INSTANCES, 'blue', attrs=['bold'])


#Guardando en archivo la cantidad minima de instancias
VARIABLE=('MIN_INSTANCES=%s' %MIN_INSTANCES)
var2file(VARIABLE,'deploy-vars')

#Guardando en archivo la cantidad maxima de instancias
VARIABLE=('MAX_INSTANCES=%s' %MAX_INSTANCES)
var2file(VARIABLE,'deploy-vars')

espacios()
#Seteo de tag
subtitulo("Seteo TAG de la version Validacion de deploy")

TAG_REV="lst"
print ("EL tag utilizado por defecto es:")
cprint(TAG_REV, 'blue', attrs=['bold'])
