# !/usr/bin/env python
# -*- coding: utf-8 -*-


#title           :gtt-cloud-run.py
#description     :Menu selector de opciones 
#version         :1.0
#usage           :python gtt-cloud-run.py
#===============================================================================

import os
from my_functions import *

#Seteos
cleanvars()

#---------------------------------------------------------------
#Chequeo de proyectos disponibles para el usuario y elegimos uno
#---------------------------------------------------------------
PROJECT=project_selector()


# #-------------------------
# #Selecionar imagen y tag
# #-------------------------
[IMAGE,IMAGE_TAG]=image_tag()


# #---------------------------------------------
# #Seleccion de SCOPE
# #---------------------------------------------
SCOPE=scope()


# #-----------------------------
# #listar variables de entorno
# #-----------------------------
os.system ("python env_selector.py")


# #-----------------------------
# # Seteos de la aplicacion
# #-----------------------------
os.system ("python set_selector.py")


# # #----------------------------------
# # #Generacion de nombre del servicio
# # #-----------------------------------
# bash ./tools/name_selector.sh


# #--------
# #Resumen
# #--------
# bash ./tools/summary.sh


# #------------------
# #Deploy desde cero
# #------------------
# bash ./tools/deploy_cr_new.sh NEW


# #------------------
# #Guardando URL
# #------------------
# bash ./tools/url_cr.sh


# #----------------------------
# #Resumen de la implementacion
# #----------------------------
# bash ./tools/cr_ws_describe.sh